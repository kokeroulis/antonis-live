package com.example.antonis_live.presentation

import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.dataProvider.DataContentEntity
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.GetContentListInteractor
import com.example.antonis_live.domain.Option
import com.example.antonis_live.presentation.contentList.ContentListPresenter
import com.example.antonis_live.presentation.contentList.ContentListView
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import java.util.*

@RunWith(RobolectricTestRunner::class)
class ContentListPresenterTest {

    private val contentModelList: List<ContentModel> by lazy {
        val data = DataContentEntity.create()
        val mapper = ContentResponseMapper()
        val entityList = mapper.to(data)
        val domainMapper = ContentEntityMapper()
        val items = domainMapper.to(entityList)
        Collections.unmodifiableList(items)
    }

    private lateinit var contentListPresenter: ContentListPresenter
    private lateinit var getContentListInteractor: GetContentListInteractor
    private lateinit var view: ContentListView

    @Before
    fun before() {
        getContentListInteractor = mock()
        view = mock()
        contentListPresenter = ContentListPresenter(getContentListInteractor)
        contentListPresenter.attach(view)
    }

    @Test
    fun loadTestSuccessFully() {
        StreamBuilder().emitItemsFromCloud(getContentListInteractor, contentModelList)
        contentListPresenter.loadData()

        val error = mock<Throwable>()
        verify(view).showLoading()
        verify(view).stopLoading()
        verify(view, never()).showError(error)
    }

    @Test
    fun whenLoadContentShowError() {
        val error = mock<Throwable>()
        StreamBuilder().emitErrorWhenFetchItems(getContentListInteractor, error)
        contentListPresenter.loadData()

        verify(view).showLoading()
        verify(view).stopLoading()
        verify(view, never()).setData(Mockito.anyList())
        verify(view).showError(error)
    }


    private class StreamBuilder {
        private val subject: BehaviorProcessor<List<ContentModel>> = BehaviorProcessor.create()

        fun emitItemsFromCloud(getContentListInteractor: GetContentListInteractor, items: List<ContentModel>) {
            whenever(getContentListInteractor.getResult(Option.None)).thenReturn(subject)
            subject.onNext(items)
        }

        fun emitErrorWhenFetchItems(getContentListInteractor: GetContentListInteractor, error: Throwable) {
            whenever(getContentListInteractor.getResult(Option.None)).thenReturn(Flowable.error(error))
        }
    }
}