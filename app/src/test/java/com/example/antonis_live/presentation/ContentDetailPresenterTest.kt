package com.example.antonis_live.presentation

import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.dataProvider.DataContentEntity
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.GetContentExtraInteractor
import com.example.antonis_live.domain.Option
import com.example.antonis_live.presentation.contentDetail.ContentDetailPresenter
import com.example.antonis_live.presentation.contentDetail.ContentDetailView
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class ContentDetailPresenterTest {

    private val contentModel: ContentModel by lazy {
        val data = DataContentEntity.create()
        val mapper = ContentResponseMapper()
        val entityList = mapper.to(data)
        val domainMapper = ContentEntityMapper()
        val items = domainMapper.to(entityList)
        items.first()
    }

    private lateinit var contentDetailsPresenter: ContentDetailPresenter
    private lateinit var getContentExtraInteractor: GetContentExtraInteractor
    private lateinit var view: ContentDetailView
    private lateinit var mapper: ContentModelMapper

    @Before
    fun before() {
        getContentExtraInteractor = mock()
        view = mock()
        mapper = ContentModelMapper()
        contentDetailsPresenter = ContentDetailPresenter(getContentExtraInteractor)
        contentDetailsPresenter.attach(view)
    }

    @Test
    fun loadTestSuccessFully() {
        StreamBuilder().emitItemsFromCloud(getContentExtraInteractor, contentModel)
        contentDetailsPresenter.loadData(ID)

        val error = mock<Throwable>()
        verify(view).showLoading()
        verify(view).stopLoading()
        verify(view, never()).showError(error)
    }

    @Test
    fun whenLoadContentShowError() {
        val error = mock<Throwable>()
        StreamBuilder().emitErrorWhenFetchItems(getContentExtraInteractor, error)
        contentDetailsPresenter.loadData(ID)

        verify(view).showLoading()
        verify(view).stopLoading()
        verify(view, never()).setData(mapper.toConvert(contentModel))
        verify(view).showError(error)
    }


    private class StreamBuilder {
        private val subject: BehaviorProcessor<ContentModel> = BehaviorProcessor.create()

        fun emitItemsFromCloud(getContentExtraInteractor: GetContentExtraInteractor, item: ContentModel) {
            whenever(getContentExtraInteractor.getResult(Option.Value(ID))).thenReturn(subject)
            subject.onNext(item)
        }

        fun emitErrorWhenFetchItems(getContentExtraInteractor: GetContentExtraInteractor, error: Throwable) {
            whenever(getContentExtraInteractor.getResult(Option.Value(ID))).thenReturn(Flowable.error(error))
        }
    }

    companion object {
        val ID = 1
    }
}