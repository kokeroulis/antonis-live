package com.example.antonis_live.data

import com.example.antonis_live.base.BaseRxTest
import com.example.antonis_live.base.Mapper
import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.data.repository.ContentDataRepository
import com.example.antonis_live.data.repository.ContentDataStore
import com.example.antonis_live.dataProvider.DataContentEntity
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.ContentRepository
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class ContentRepositoryTest : BaseRxTest() {


    private lateinit var repository: ContentRepository
    private lateinit var localDataStore: ContentDataStore
    private lateinit var remoteDataStore: ContentDataStore
    private lateinit var mapper: Mapper<ContentEntity, ContentModel>

    private val entityList: List<ContentEntity> by lazy {
        val data = DataContentEntity.create()
        val mapper = ContentResponseMapper()
        val entityList = mapper.to(data)
        Collections.unmodifiableList(entityList)
    }

    @Before
    fun before() {
        localDataStore = mock()
        remoteDataStore = mock()
        mapper = ContentEntityMapper()
        repository = ContentDataRepository(localDataStore, remoteDataStore, mapper)
    }

    @Test
    fun emitItemsFromRepo() {
        StreamBuilder().emitItems(localDataStore, remoteDataStore, entityList)
        val subscriber = repository.getContent().test()
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
    }

    @Test
    fun getItemsFromCloudAndSaveToDb() {
        StreamBuilder().emitItemsFromCloud(localDataStore, remoteDataStore, entityList)
        val subscriber = repository.getContent().test()
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
    }

    @Test
    fun updateContentItem() {
        val itemWithBody = StreamBuilder().updateItemFromRepo(localDataStore, remoteDataStore, entityList)
        val subscriber = repository.getContentById(itemWithBody.id).test()
        subscriber.assertNotComplete()
        subscriber.assertNoErrors()
        subscriber.assertValueCount(1)
        subscriber.assertValue(mapper.toConvert(itemWithBody))
    }

    private class StreamBuilder {
        private val subject: BehaviorProcessor<List<ContentEntity>> = BehaviorProcessor.create()


        fun emitItems(localDataStore: ContentDataStore,
                      remoteContentDataStore: ContentDataStore,
                      items: List<ContentEntity>) {

            whenever(localDataStore.getContent()).thenReturn(subject)
            verify(remoteContentDataStore, never()).getContent()
            subject.onNext(items)
        }

        fun emitItemsFromCloud(localDataStore: ContentDataStore,
                               remoteContentDataStore: ContentDataStore,
                               items: List<ContentEntity>) {
            whenever(localDataStore.getContent()).thenReturn(subject)
            whenever(remoteContentDataStore.getContent()).thenReturn(Flowable.just(items))
            whenever(localDataStore.saveContent(Mockito.anyList())).thenReturn(Completable.complete())

            // show that we have no data in the memory
            subject.onNext(listOf())
        }

        fun updateItemFromRepo(localDataStore: ContentDataStore,
                               remoteContentDataStore: ContentDataStore,
                               items: List<ContentEntity>) : ContentEntity {

            val item = items.first()
            whenever(localDataStore.getContent()).thenReturn(subject)
            val itemWithBody = item.copy(body = "Some body")
            whenever(remoteContentDataStore.getContentById(item.id)).thenReturn(Flowable.just(itemWithBody))
            whenever(localDataStore.updateContent(itemWithBody)).thenReturn(Completable.complete())
            subject.onNext(items)

            doAnswer {
                subject.onNext(listOf(itemWithBody))
                Completable.complete()
            }.whenever(localDataStore).updateContent(itemWithBody)
            return itemWithBody
        }
    }
}