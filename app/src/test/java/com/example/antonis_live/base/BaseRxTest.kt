package com.example.antonis_live.base

import org.junit.Rule
import org.mockito.junit.MockitoJUnit

abstract class BaseRxTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()

    @Rule
    @JvmField
    val rxOverrideScheduler = RxOverrideScheduler()
}