package com.example.antonis_live.domain

import com.example.antonis_live.base.BaseRxTest
import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.dataProvider.DataContentEntity
import com.example.antonis_live.domain.GetContentListInteractorTest.StreamBuilder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import java.io.IOException

class GetContentExtraInteractorTest : BaseRxTest() {

    private val contentModel: ContentModel by lazy {
        val data = DataContentEntity.create()
        val mapper = ContentResponseMapper()
        val entityList = mapper.to(data)
        val domainMapper = ContentEntityMapper()
        val items = domainMapper.to(entityList)
        items.first()
    }

    private lateinit var repository: ContentRepository
    private lateinit var contentExtraInteractor: GetContentExtraInteractor

    @Before
    fun before() {
        repository = mock()
        contentExtraInteractor = GetContentExtraInteractor(repository)
    }

    @Test
    fun getItemsFromCache() {
        StreamBuilder().getItemsFromCache(repository, contentModel)

        val subscriber = contentExtraInteractor.getResult(Option.Value(ID)).test()
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
    }

    @Test
    fun fetchContentListAndFailOnNetwork() {
        val error = IOException("Bad internet connection")
        StreamBuilder().getItemsFromCloudButFailOnNetwork(repository, error)

        val subscriber = contentExtraInteractor.getResult(Option.Value(ID)).test()
        subscriber.assertError(error)
    }

    class StreamBuilder {
        private val subject: BehaviorProcessor<ContentModel> = BehaviorProcessor.create()

        fun getItemsFromCache(repository: ContentRepository, contentList: ContentModel) {
            whenever(repository.getContentById(ID)).thenReturn(subject)

            subject.onNext(contentList)
        }

        fun getItemsFromCloudButFailOnNetwork(repository: ContentRepository, error: Throwable) {
            whenever(repository.getContentById(ID)).thenReturn(Flowable.error(error))
        }
    }

    companion object {
        val ID = 1
    }
}
