package com.example.antonis_live.domain

import com.example.antonis_live.base.BaseRxTest
import com.example.antonis_live.data.ContentEntity
import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.dataProvider.DataContentEntity
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import java.io.IOException
import java.util.*

class GetContentListInteractorTest : BaseRxTest() {

    private val contentList: List<ContentModel> by lazy {
        val data = DataContentEntity.create()
        val mapper = ContentResponseMapper()
        val entityList = mapper.to(data)
        val domainMapper = ContentEntityMapper()
        val items = domainMapper.to(entityList)
        Collections.unmodifiableList(items)
    }

    private lateinit var repository: ContentRepository
    private lateinit var contentListInteractor: GetContentListInteractor

    @Before
    fun before() {
        repository = mock()
        contentListInteractor = GetContentListInteractor(repository)
    }

    @Test
    fun getItemsFromCache() {
        StreamBuilder().getItemsFromCache(repository, contentList)
        val subscriber = contentListInteractor.getResult(Option.None).test()

        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
    }

    @Test
    fun fetchContentListAndFailOnNetwork() {
        val error = IOException("Bad internet connection")
        StreamBuilder().getItemsFromCloudButFailOnNetwork(repository, error)

        val subscriber = contentListInteractor.getResult(Option.None).test()
        subscriber.assertError(error)
    }

    class StreamBuilder {
        private val subject: BehaviorProcessor<List<ContentModel>> = BehaviorProcessor.create()

        fun getItemsFromCache(repository: ContentRepository, contentList: List<ContentModel>) {
            whenever(repository.getContent()).thenReturn(subject)

            subject.onNext(contentList)
        }

        fun getItemsFromCloudButFailOnNetwork(repository: ContentRepository, error: Throwable) {
            whenever(repository.getContent()).thenReturn(Flowable.error(error))
        }
    }
}