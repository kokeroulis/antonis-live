package com.example.antonis_live.dataProvider

import com.example.antonis_live.data.network.ContentListResponse

object DataContentEntity {

    fun create(): List<ContentListResponse> {
        return listOf(
            ContentListResponse(1, "title 1", "subtitle 1", "23/07/1991", null),
            ContentListResponse(2, "title 2", "subtitle 2", "23/07/1991", null),
            ContentListResponse(3, "title 3", "subtitle 3", "23/07/1991", null),
            ContentListResponse(4, "title 4", "subtitle 4", "23/07/1991", null)
        )
    }
}