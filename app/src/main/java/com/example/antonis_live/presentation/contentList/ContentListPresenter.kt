package com.example.antonis_live.presentation.contentList

import android.util.Log
import com.example.antonis_live.base.Mapper
import com.example.antonis_live.base.MvpPresenter
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.GetContentListInteractor
import com.example.antonis_live.domain.Option
import com.example.antonis_live.presentation.ContentItem
import com.example.antonis_live.presentation.ContentModelMapper
import io.reactivex.android.schedulers.AndroidSchedulers

class ContentListPresenter(
    private val getContentListInteractor: GetContentListInteractor,
    private val mapper: Mapper<ContentModel, ContentItem> = ContentModelMapper()
) : MvpPresenter<ContentListView>() {

    fun loadData() {
        compositeDisposable += getContentListInteractor.getResult(Option.None)
            .map { mapper.to(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                getMvpView()?.showLoading()
            }
            .subscribe(
                { next ->

                    Log.e("test", next.toString())
                    getMvpView()?.stopLoading()
                    getMvpView()?.setData(next)
                },
                { error ->
                    getMvpView()?.stopLoading()
                    getMvpView()?.showError(error)
                }
            )
    }

    override fun deAttach() {
        super.deAttach()
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable?.dispose()
            compositeDisposable = null
        }
    }

}