package com.example.antonis_live.presentation.contentList

import com.example.antonis_live.base.MvpView
import com.example.antonis_live.presentation.ContentItem

interface ContentListView : MvpView {

    fun setData(contentList: List<ContentItem>)

    fun showLoading()

    fun stopLoading()

    fun showError(throwable: Throwable)
}