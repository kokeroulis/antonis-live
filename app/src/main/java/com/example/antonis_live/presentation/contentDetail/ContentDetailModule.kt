package com.example.antonis_live.presentation.contentDetail

import com.example.antonis_live.dagger.scopes.FragmentScope
import com.example.antonis_live.domain.GetContentExtraInteractor
import com.example.antonis_live.domain.GetContentListInteractor
import dagger.Module
import dagger.Provides

@Module
class ContentDetailModule {

    @Provides
    @FragmentScope
    fun providesContentDetailPresenter(getContentExtraInteractor: GetContentExtraInteractor): ContentDetailPresenter {
        return ContentDetailPresenter(getContentExtraInteractor)
    }
}