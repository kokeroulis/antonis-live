package com.example.antonis_live.presentation.contentDetail

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.example.antonis_live.R
import com.example.antonis_live.base.MvpFragment
import com.example.antonis_live.presentation.ContentItem

class ContentDetailFragment : MvpFragment<ContentDetailView, ContentDetailPresenter>(), ContentDetailView {

    override val layoutRes = R.layout.fragment_content_detail

    val containerDetail: LinearLayout by bindView(R.id.container_detail)
    val articleTitleView: TextView by bindView(R.id.articleTitle)
    val articleSubTitleView: TextView by bindView(R.id.articleSubtitle)
    val articleBodyView: TextView by bindView(R.id.articleTitle)
    val progressBarView: ProgressBar by bindView(R.id.progressBar)

    override fun getMvpView() = this

    override fun onFirstRun() {
        val id = arguments.getInt(ID_ARGUMENT, -1)
        presenter.loadData(id)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle("Content Detail")
    }

    override fun showError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun setData(contentItem: ContentItem) {
        articleTitleView.text = contentItem.title
        articleSubTitleView.text = contentItem.subTitle
        articleBodyView.text = contentItem.body
    }

    override fun showLoading() {
        progressBarView.visibility = View.VISIBLE
        containerDetail.visibility = View.GONE
    }

    override fun stopLoading() {
        progressBarView.visibility = View.GONE
        containerDetail.visibility = View.VISIBLE
    }

    companion object {

        val TAG = ContentDetailFragment::class.java.simpleName
        val ID_ARGUMENT = "id_argument"

        fun instance(id: Int): ContentDetailFragment {
            val fragment = ContentDetailFragment()
            val args = Bundle()
            args.putInt(ID_ARGUMENT, id)
            fragment.arguments = args

            return fragment
        }
    }
}