package com.example.antonis_live.presentation

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.domain.ContentModel

class ContentModelMapper : Mapper<ContentModel, ContentItem> {

    override fun fromConvert(to: ContentItem): ContentModel {
        return ContentModel(to.id, to.title, to.subTitle, to.date, to.body)
    }

    override fun toConvert(from: ContentModel): ContentItem {
        return ContentItem(from.id, from.title, from.subTitle, from.date, from.body)
    }
}