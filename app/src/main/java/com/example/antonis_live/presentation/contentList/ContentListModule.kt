package com.example.antonis_live.presentation.contentList

import com.example.antonis_live.dagger.scopes.FragmentScope
import com.example.antonis_live.domain.GetContentListInteractor
import dagger.Module
import dagger.Provides

@Module
class ContentListModule {

    @Provides
    @FragmentScope
    fun providesContentListPresenter(getContentListInteractor: GetContentListInteractor): ContentListPresenter {
        return ContentListPresenter(getContentListInteractor)
    }
}