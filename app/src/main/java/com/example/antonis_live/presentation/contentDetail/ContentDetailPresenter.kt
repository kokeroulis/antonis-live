package com.example.antonis_live.presentation.contentDetail

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.base.MvpPresenter
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.GetContentExtraInteractor
import com.example.antonis_live.domain.Option
import com.example.antonis_live.presentation.ContentItem
import com.example.antonis_live.presentation.ContentModelMapper
import io.reactivex.android.schedulers.AndroidSchedulers

class ContentDetailPresenter(
    private val getContentListInteractor: GetContentExtraInteractor,
    private val mapper: Mapper<ContentModel, ContentItem> = ContentModelMapper()
) : MvpPresenter<ContentDetailView>() {

    fun loadData(id: Int) {
        compositeDisposable += getContentListInteractor.getResult(Option.Value(id))
            .map { mapper.toConvert(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                getMvpView()?.showLoading()
            }
            .subscribe(
                { next ->
                    getMvpView()?.stopLoading()
                    getMvpView()?.setData(next)
                },
                { error ->
                    getMvpView()?.stopLoading()
                    getMvpView()?.showError(error)
                }
            )
    }

    override fun deAttach() {
        super.deAttach()
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable?.dispose()
            compositeDisposable = null
        }
    }
}