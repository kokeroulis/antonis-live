package com.example.antonis_live.presentation.contentDetail

import com.example.antonis_live.base.MvpView
import com.example.antonis_live.presentation.ContentItem

interface ContentDetailView : MvpView {

    fun setData(contentItem: ContentItem)

    fun showLoading()

    fun stopLoading()

    fun showError(throwable: Throwable)
}