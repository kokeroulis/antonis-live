package com.example.antonis_live.presentation

data class ContentItem(
    val id: Int,
    val title: String,
    val subTitle: String,
    val date: String,
    val body: String?
)