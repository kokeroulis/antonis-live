package com.example.antonis_live.presentation.contentList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.antonis_live.R
import com.example.antonis_live.presentation.ContentItem
import com.example.antonis_live.presentation.contentList.ContentListAdapter.ContentListViewHolder

class ContentListAdapter : RecyclerView.Adapter<ContentListViewHolder>() {

    var contentModelList: List<ContentItem>? = null

    var onContentSelected: (contentId: Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.holder_content_list, parent, false)
        return ContentListViewHolder(view)
    }


    override fun onBindViewHolder(holder: ContentListViewHolder, position: Int) {
        val model = contentModelList?.get(position)
        model?.let {
            holder.bind(it)
        }
        holder.itemView.setOnClickListener {
            val pos = holder.adapterPosition
            val modelSelected = contentModelList?.get(pos)
            modelSelected?.let {
                onContentSelected(modelSelected.id)
            }
        }
    }

    override fun getItemCount(): Int {
        return contentModelList?.size?: 0
    }

    class ContentListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val titleView: TextView = itemView.findViewById(R.id.articleTitle)
        private val subTitleView: TextView = itemView.findViewById(R.id.articleSubtitle)

        fun bind(model: ContentItem) {
            titleView.text = model.title
            subTitleView.text = model.subTitle
        }
    }
}