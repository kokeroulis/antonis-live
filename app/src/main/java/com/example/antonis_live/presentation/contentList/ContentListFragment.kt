package com.example.antonis_live.presentation.contentList

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.example.antonis_live.R
import com.example.antonis_live.base.MvpFragment
import com.example.antonis_live.presentation.ContentItem
import com.example.antonis_live.presentation.contentDetail.ContentDetailFragment

class ContentListFragment : MvpFragment<ContentListView, ContentListPresenter>(), ContentListView {

    override val layoutRes = R.layout.fragment_content__list

    val recyclerView: RecyclerView by bindView(R.id.rv)
    val progressBarView: ProgressBar by bindView(R.id.progressBar)

    private val adapter = ContentListAdapter()

    override fun getMvpView() = this

    override fun onFirstRun() {
        presenter.loadData()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle("Content List")
        initRecyclerView()
    }

    override fun showError(throwable: Throwable) {
        Toast.makeText(context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    override fun setData(contentList: List<ContentItem>) {
        adapter.contentModelList = contentList
        adapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        progressBarView.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun stopLoading() {
        progressBarView.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    private fun initRecyclerView() {
        val lm = LinearLayoutManager(context)
        recyclerView.layoutManager = lm
        recyclerView.adapter = adapter

        adapter.onContentSelected = { id ->
            val appActivity = activity as? AppCompatActivity
            appActivity?.let {
                val fragment = ContentDetailFragment.instance(id)
                appActivity.supportFragmentManager.beginTransaction()
                    .add(R.id.container, fragment, ContentDetailFragment.TAG)
                    .addToBackStack(ContentDetailFragment.TAG)
                    .commit()
            }
        }
    }

    companion object {

        val TAG = ContentListFragment::class.java.simpleName
    }
}