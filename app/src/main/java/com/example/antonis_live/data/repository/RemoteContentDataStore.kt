package com.example.antonis_live.data.repository

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.data.ContentEntity
import com.example.antonis_live.data.mapper.ContentResponseMapper
import com.example.antonis_live.data.network.ContentListResponse
import com.example.antonis_live.data.network.ContentService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

class RemoteContentDataStore(
    private val contentService: ContentService,
    private val mapper: Mapper<ContentListResponse, ContentEntity> = ContentResponseMapper()
) : ContentDataStore {

    override fun getContent(): Flowable<List<ContentEntity>> {
       return contentService.getContentList()
           .subscribeOn(Schedulers.io())
           .map { mapper.to(it) }
           .toFlowable()
    }

    override fun getContentById(id: Int): Flowable<ContentEntity> {
        return contentService.getContentById(id)
            .subscribeOn(Schedulers.io())
            .map { mapper.toConvert(it) }
            .toFlowable()
    }

    override fun saveContent(contentList: List<ContentEntity>): Completable {
        throw IllegalArgumentException("This operation is not permitted here")
    }

    override fun updateContent(content: ContentEntity): Completable {
        throw IllegalArgumentException("This operation is not permitted here")
    }
}