package com.example.antonis_live.data.network

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class ContentServiceImpl : ContentService {

    private val gson: Gson = Gson()


    override fun getContentList(): Single<List<ContentListResponse>> {
        return Single.create {
            val res = performNetworkRequest("${API_ENDPOINT}contentList.json")
            if (res == null) {
                it.onError(IllegalArgumentException("We shouldn't get an empty response"))
            }

            val type = object : TypeToken<Map<String, Any>>() {}.type
            val mapObject = gson.fromJson<Map<String, Any>>(res, type)
            val mapResp = mapObject["items"]
            val listType = object : TypeToken<List<Map<String, Any>>>() {}.type
            val itemsString = gson.toJson(mapResp, listType)
            val responseType = object : TypeToken<List<ContentListResponse>>() {}.type
            val response = gson.fromJson<List<ContentListResponse>>(itemsString, responseType)
            it.onSuccess(response)
        }
    }

    override fun getContentById(id: Int): Single<ContentListResponse> {
        return Single.create {
            val res = performNetworkRequest("${API_ENDPOINT}content/${id}.json")
            if (res == null) {
                it.onError(IllegalArgumentException("We shouldn't get an empty response"))
            }

            val type = object : TypeToken<Map<String, Any>>() {}.type
            val mapObject = gson.fromJson<Map<String, Any>>(res, type)
            val mapResp = mapObject["item"]
            val itemsString = gson.toJson(mapResp, type)
            val responseType = object : TypeToken<ContentListResponse>() {}.type
            val response = gson.fromJson<ContentListResponse>(itemsString, responseType)
            it.onSuccess(response)
        }
    }

    private fun performNetworkRequest(urlString: String): String? {
        val url: URL = URL(urlString)
        var urlConnection: HttpURLConnection? = null

        urlConnection = url.openConnection() as HttpURLConnection
        var response: String? = null
        val responseCode = urlConnection.responseCode
        if (responseCode == HttpURLConnection.HTTP_OK) {
            response = readStream(urlConnection.inputStream)
        }

        return response
    }

    private fun readStream(input: InputStream): String {
        var reader: BufferedReader? = null
        val response = StringBuffer()
        try {
            reader = BufferedReader(InputStreamReader(input))
            var line = reader.readLine()
            while (line != null) {
                response.append(line)
                line = reader.readLine()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
        return response.toString()
    }

    companion object {
        val API_ENDPOINT = "http://dynamic.pulselive.com/test/native/"
    }
}