package com.example.antonis_live.data.repository

import com.example.antonis_live.data.ContentEntity
import io.reactivex.Completable
import io.reactivex.Flowable

interface ContentDataStore {

    fun getContent(): Flowable<List<ContentEntity>>

    fun getContentById(id: Int): Flowable<ContentEntity>

    fun saveContent(contentList: List<ContentEntity>): Completable

    fun updateContent(content: ContentEntity): Completable
}