package com.example.antonis_live.data.repository

import com.example.antonis_live.data.network.ContentService
import com.example.antonis_live.domain.ContentRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ContentModule {

    @Provides
    @Singleton
    @Named(LOCAL_STORE)
    fun providesLocalContentDataStore(): ContentDataStore {
        return MemoryContentDataResponse()
    }

    @Provides
    @Singleton
    @Named(REMOTE_STORE)
    fun providesRemoteContentDataStore(contentService: ContentService): ContentDataStore {
        return RemoteContentDataStore(contentService)
    }


    @Provides
    @Singleton
    fun providesContentRepository(
        @Named(LOCAL_STORE) localDataStore: ContentDataStore,
        @Named(REMOTE_STORE) remoteDataStore: ContentDataStore
    ): ContentRepository {
        return ContentDataRepository(localDataStore, remoteDataStore)
    }

    companion object {
        const val LOCAL_STORE = "local_store"
        const val REMOTE_STORE = "remote_store"
    }
}