package com.example.antonis_live.data.mapper

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.data.ContentEntity
import com.example.antonis_live.domain.ContentModel

class ContentEntityMapper : Mapper<ContentEntity, ContentModel> {

    override fun fromConvert(to: ContentModel): ContentEntity {
        return ContentEntity(to.id, to.title, to.subTitle, to.date, to.body)
    }

    override fun toConvert(from: ContentEntity): ContentModel {
        return ContentModel(from.id, from.title, from.subTitle, from.date, from.body)
    }
}