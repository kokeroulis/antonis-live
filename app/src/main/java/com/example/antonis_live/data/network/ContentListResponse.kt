package com.example.antonis_live.data.network

import com.google.gson.annotations.SerializedName

data class ContentListResponse(val id: Int,
                               val title: String,
                               @SerializedName("subtitle")
                               val subTitle: String,
                               val date: String,
                               val body: String?)