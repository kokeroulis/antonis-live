package com.example.antonis_live.data.mapper

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.data.ContentEntity
import com.example.antonis_live.data.network.ContentListResponse

class ContentResponseMapper : Mapper<ContentListResponse, ContentEntity> {
    override fun fromConvert(to: ContentEntity): ContentListResponse {
        return ContentListResponse(to.id, to.title, to.subTitle, to.date, to.body)
    }

    override fun toConvert(from: ContentListResponse): ContentEntity {
        return ContentEntity(from.id, from.title, from.subTitle, from.date, from.body)
    }
}