package com.example.antonis_live.data

data class ContentEntity(
    val id: Int,
    val title: String,
    val subTitle: String,
    val date: String,
    val body: String?
)