package com.example.antonis_live.data.repository

import com.example.antonis_live.base.Mapper
import com.example.antonis_live.data.ContentEntity
import com.example.antonis_live.data.mapper.ContentEntityMapper
import com.example.antonis_live.domain.ContentModel
import com.example.antonis_live.domain.ContentRepository
import io.reactivex.Completable
import io.reactivex.Flowable

class ContentDataRepository(
    private val localDataStore: ContentDataStore,
    private val remoteContentDataStore: ContentDataStore,
    private val mapper: Mapper<ContentEntity, ContentModel> = ContentEntityMapper()
) : ContentRepository {

    override fun getContent(): Flowable<List<ContentModel>> {
        return localDataStore.getContent()
            .map { mapper.to(it) }
            .flatMap { loadDataInEmpty(it) }
            .filter { it.isNotEmpty() }
    }

    override fun getContentById(id: Int): Flowable<ContentModel> {
        return getContent()
            .map { it.filter { it.id == id } }
            .filter { it.isNotEmpty() }
            .map { it.first() }
            .flatMap { loadExtraInfo(it) }
            .filter { it.hasExtraInfo() }
    }

    override fun saveContent(contentList: List<ContentModel>): Completable {
        return localDataStore.saveContent(mapper.from(contentList))
    }

    private fun loadDataInEmpty(candidateContentList: List<ContentModel>): Flowable<List<ContentModel>> {
        return if (candidateContentList.isEmpty()) {
            remoteContentDataStore.getContent()
                .flatMapCompletable { localDataStore.saveContent(it) }
                .andThen(Flowable.just(candidateContentList))
        } else Flowable.just(candidateContentList)
    }

    private fun loadExtraInfo(contentModel: ContentModel): Flowable<ContentModel> {
        return if (!contentModel.hasExtraInfo()) {
            remoteContentDataStore.getContentById(contentModel.id)
                .flatMapCompletable { localDataStore.updateContent(it) }
                .andThen(Flowable.just(contentModel))
        } else Flowable.just(contentModel)
    }
}