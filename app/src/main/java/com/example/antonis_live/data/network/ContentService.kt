package com.example.antonis_live.data.network

import io.reactivex.Single

interface ContentService {

    fun getContentList(): Single<List<ContentListResponse>>

    fun getContentById(id: Int): Single<ContentListResponse>
}