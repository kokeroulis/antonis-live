package com.example.antonis_live.data.repository

import android.util.Log
import com.example.antonis_live.data.ContentEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers

class MemoryContentDataResponse : ContentDataStore {

    private val subjectCache: BehaviorProcessor<List<ContentEntity>> = BehaviorProcessor.create()

    init {
        // add an initial value in order to be able to separate the abcense of data
        subjectCache.onNext(listOf())
    }

    override fun getContent(): Flowable<List<ContentEntity>> {
        return subjectCache
    }

    override fun getContentById(id: Int): Flowable<ContentEntity> {
       return subjectCache
           .map { it.filter { it.id == id } }
           .filter { it.isNotEmpty() }
           .map { it.first() }
    }

    override fun saveContent(contentList: List<ContentEntity>): Completable {
        // this could also run outside of a Completable
        // or without caring about the operation thread
        // but we do it this way, in order to show what we should
        // have done if we had a database
        return Completable.create {
            subjectCache.onNext(contentList)
            it.onComplete()
        }
        .subscribeOn(Schedulers.io())
    }

    override fun updateContent(content: ContentEntity): Completable {
        return Completable.create {
            val items = subjectCache.value.toMutableList()
            val contentCandidate = items.find { it.id == content.id }

            contentCandidate?.let {
                val idx = items.indexOf(contentCandidate)
                items.set(idx, content)
                subjectCache.onNext(items)
            }
            it.onComplete()
        }
    }

}