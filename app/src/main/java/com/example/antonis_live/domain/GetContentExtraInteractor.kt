package com.example.antonis_live.domain

import io.reactivex.Flowable
import javax.inject.Inject

class GetContentExtraInteractor @Inject constructor(
    private val contentRepository: ContentRepository
) : Interactor<Int, ContentModel> {

    override fun getResult(param: Option<Int>): Flowable<ContentModel> {
        val value = param as? Option.Value
        return value?.let {
            return contentRepository.getContentById(value.item)
        } ?: Flowable.error(IllegalArgumentException("You must pass a value inside this interactor"))
    }
}