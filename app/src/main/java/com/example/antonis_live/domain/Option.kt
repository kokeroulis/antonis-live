package com.example.antonis_live.domain

sealed class Option<Param> {

    data class Value<Param>(val item: Param) : Option<Param>()

    object None : Option<Any>()
}