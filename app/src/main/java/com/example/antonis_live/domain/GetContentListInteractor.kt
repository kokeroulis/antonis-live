package com.example.antonis_live.domain

import io.reactivex.Flowable
import javax.inject.Inject

class GetContentListInteractor @Inject constructor(
    private val contentRepository: ContentRepository
) : Interactor<Any, List<ContentModel>> {

    override fun getResult(param: Option<Any>): Flowable<List<ContentModel>> {
        return contentRepository.getContent()
    }
}