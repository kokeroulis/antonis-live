package com.example.antonis_live.domain

import io.reactivex.Completable
import io.reactivex.Flowable

interface ContentRepository {

    fun getContent(): Flowable<List<ContentModel>>

    fun getContentById(id: Int): Flowable<ContentModel>

    fun saveContent(contentList: List<ContentModel>): Completable
}