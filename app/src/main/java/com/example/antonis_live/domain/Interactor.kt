package com.example.antonis_live.domain

import io.reactivex.Flowable

interface Interactor<Param, Result> {

    fun getResult(param: Option<Param>): Flowable<Result>
}