package com.example.antonis_live.domain

data class ContentModel(
    val id: Int,
    val title: String,
    val subTitle: String,
    val date: String,
    val body: String?
) {

    fun hasExtraInfo(): Boolean {
        return !body.isNullOrEmpty()
    }
}
