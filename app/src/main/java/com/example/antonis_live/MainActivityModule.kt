package com.example.antonis_live

import com.example.antonis_live.dagger.scopes.FragmentScope
import com.example.antonis_live.presentation.contentDetail.ContentDetailFragment
import com.example.antonis_live.presentation.contentDetail.ContentDetailModule
import com.example.antonis_live.presentation.contentList.ContentListFragment
import com.example.antonis_live.presentation.contentList.ContentListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = arrayOf(ContentListModule::class))
    abstract fun contributesContentListFragmentInjector(): ContentListFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = arrayOf(ContentDetailModule::class))
    abstract fun contributesContentDetailFragmentInjector(): ContentDetailFragment
}