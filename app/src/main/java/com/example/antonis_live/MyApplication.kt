package com.example.antonis_live

import android.app.Activity
import android.app.Application
import com.example.antonis_live.dagger.component.ApplicationComponent
import com.example.antonis_live.dagger.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MyApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    private val component: ApplicationComponent by lazy {
        createComponent()
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

    private fun createComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder()
            .build()
    }
}