package com.example.antonis_live.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import javax.inject.Provider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class MvpFragment<MVPVIEW: MvpView, PRESENTER: MvpPresenter<MVPVIEW>> : Fragment() {

    protected val presenter: PRESENTER by lazy {
        createPresenter()
    }

    @Inject lateinit var presenterProvider: Provider<PRESENTER>

    protected open fun createPresenter(): PRESENTER {
        return presenterProvider.get()
    }

    abstract fun getMvpView(): MVPVIEW

    abstract val layoutRes: Int

    protected open fun onFirstRun() {}

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        onFirstRun()
    }

    protected val actionBar: ActionBar?
        get() = (activity as? AppCompatActivity)?.supportActionBar

    protected fun setTitle(title: String) {
        actionBar?.title = title
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(getMvpView())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.deAttach()
    }

    fun <V: View> Fragment.bindView(id: Int) : ReadOnlyProperty<Fragment, V> {
        return FindViewByIdDelegate(id)
    }

    private class FindViewByIdDelegate <V: View> (
        private val id: Int
    ) : ReadOnlyProperty<Fragment, V> {

        override fun getValue(thisRef: Fragment, property: KProperty<*>): V {
            return thisRef.view?.findViewById<V>(id)
                ?: throw IllegalArgumentException("View is not bounded yet for fragment ${thisRef.tag}")
        }

    }
}