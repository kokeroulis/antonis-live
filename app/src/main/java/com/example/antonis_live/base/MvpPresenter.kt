package com.example.antonis_live.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

abstract class MvpPresenter<MVPVIEW: MvpView> {

    private var view: WeakReference<MVPVIEW>? = null
    protected var compositeDisposable: CompositeDisposable? = null

    fun attach(mvpView: MVPVIEW) {
        view = WeakReference(mvpView)
    }

    open fun deAttach() {
        view = null
    }

    fun getMvpView(): MVPVIEW? {
        return view?.get()
    }

    operator fun CompositeDisposable?.plusAssign(disposable: Disposable) {
        if (compositeDisposable == null || disposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

        compositeDisposable!!.add(disposable)
    }
}