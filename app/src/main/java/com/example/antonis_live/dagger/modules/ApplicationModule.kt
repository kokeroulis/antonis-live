package com.example.antonis_live.dagger.modules

import com.example.antonis_live.data.network.ContentService
import com.example.antonis_live.data.network.ContentServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun providesContentServer(): ContentService {
        return ContentServiceImpl()
    }
}