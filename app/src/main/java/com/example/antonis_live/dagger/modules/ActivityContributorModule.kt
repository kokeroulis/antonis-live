package com.example.antonis_live.dagger.modules

import com.example.antonis_live.MainActivity
import com.example.antonis_live.MainActivityModule
import com.example.antonis_live.dagger.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityContributorModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun contributesMainActivityInjector(): MainActivity
}
