package com.example.antonis_live.dagger.component

import com.example.antonis_live.MyApplication
import com.example.antonis_live.dagger.modules.ActivityContributorModule
import com.example.antonis_live.dagger.modules.ApplicationModule
import com.example.antonis_live.data.repository.ContentModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    ActivityContributorModule::class,
    ApplicationModule::class,
    ContentModule::class)
)
interface ApplicationComponent {


    fun inject(application: MyApplication)
}